# CentBee-CRUD-Ionic

## Project Assessment :

Create an Ionic 4 Angular CRUD application to maintain a list of movies. The API needs to be hosted on AWS (Free account available) and developed using NodeJS and Express. Check all source code into Github or Bitbucket and include a README with instructions to run the application. Use postgresql as your database. Make sure to follow good design patterns and best practices.

## Technologies
* Babel - Please make sure that this package is installed using the following command
*npm install -g babel-cli*
* JavaScript (ES6)
* NodeJs
* Express
* Postgresql
* PG

# Steps
## Step 1(Building the api located in the movies-api directory )
### Run the following command to install all the required packages(Expressjs, momentjs, babeljs, babel watch)

*npm install --save express moment uuid*
*npm install --save-dev babel-cli babel-preset-env babel-watch*

### Run the following command
*npm run build*
*npm start*

### API Server Running
If the above command is successful then you should see the api by accessing the URL [http://localhost:3000]

## Step 2: Ionic app build:
### In the moviesCrud directory, run the following command
*cd moviesCrud*
*ionic serve -l*

This will open the browser with the preview of your app which will reload automatically once you change anything inside your project.

## Ionic Lab
If you want to get a better feeling for how your app will look on a real device you can also run ionic lab instead of serve but you have to install the package upfront:
*# Install the Lab Package*
*npm i @ionic/lab*
*# Run your app with device preview and platform styles*
*ionic lab*

## AWS URL Database Credentials
Endpoint:        centmoviesround2.c0lho1lyyopu.eu-west-2.rds.amazonaws.com
Port:            5432
Database:        themovielist
Master Username: sammysa
Master Password: C3ntB3e2019MasterPAssW0rd



# OMDb - Not relevant for now -
# Note
The simple movies app with CRUD functionality has been created under the *moviesCRUD* directory whereas the *moviesApp* is one where we wanted to intergrate into the OMDBAPI[http://www.omdbapi.com/], which is still a WIP currenty

## OMDb API Key
Key: 52d3e17c

OMDb API: [http://www.omdbapi.com/?i=tt3896198&apikey=52d3e17c]

## Breakdown

### Running the project locally
Please make sure that you have the following installed
* NodeJS
* NPM
* Angular CLI: npm install -g @angular/cli

### Personal note
*To be honest, this assignment was/is harder than anticipated. Not impossible, just have a million questions. Will be rebuilding it in another way though and will update the repo over the weekend if interested.*

*Even if it fails, I will not quit on it*