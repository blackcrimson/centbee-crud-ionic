import { Component, OnInit } from '@angular/core';

import { LoadingController } from '@ionic/angular';
import { RestApiService } from '../rest-api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  movie: any = {};
  location: any;

  constructor(public api: RestApiService,
    public loadingController: LoadingController,
    public route: ActivatedRoute,
    public router: Router) { }

  ngOnInit() {
    this.getMovie();
  }

  async getMovie() {
    const loading = await this.loadingController.create({
      message: 'Loading Details Page'
    });
    await loading.present();
    await this.api.getMovieById(this.route.snapshot.paramMap.get('id'))
      .subscribe(res => {
        console.log(res);
        this.movie = res;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  async delete(id) {
    const loading = await this.loadingController.create({
      message: 'Deleting Movie'
    });
    await loading.present();
    await this.api.deleteMovie(id)
      .subscribe(res => {
        loading.dismiss();
        this.location.back();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }
}
