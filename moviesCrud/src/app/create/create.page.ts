import { Component, OnInit } from '@angular/core';

import { LoadingController } from '@ionic/angular';
import { RestApiService } from '../rest-api.service';
import { ActivatedRoute, Router  } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {
  // getMovie(arg0: string) {
  //   throw new Error("Method not implemented.");
  // }

  movieForm: FormGroup;
  genre: FormArray;

  constructor(public api: RestApiService,
    public loadingController: LoadingController,
    private route: ActivatedRoute,
    public router: Router,
    private formBuilder: FormBuilder) {
      this.getMovie(this.route.snapshot.paramMap.get('id'));
      this.movieForm = this.formBuilder.group({
      'movie_name' : [null, Validators.required],
      'genre' : this.formBuilder.array([])
    });
  }

  ngOnInit() {
  }

  createGenre(): FormGroup {
    return this.formBuilder.group({
      student_name: ''
    });
  }

  addBlankGenre(): void {
    this.genre = this.movieForm.get('genre') as FormArray;
    this.genre.push(this.createGenre());
  }

  deleteGenre({ control, index }: { control; index; }) {
    control.removeAt(index);
  }

  async saveMovie() {
    await this.api.postMovie(this.movieForm.value)
    .subscribe(res => {
        const id = res['id'];
        this.router.navigate(['/detail/' + id]);
      }, (err) => {
        console.log(err);
      });
  }
  async getMovie(id) {
    const loading = await this.loadingController.create({
      message: 'Time to create a movie'
    });
    await loading.present();
    await this.api.getMovieById(this.route.snapshot.paramMap.get('id'))
      .subscribe(res => {
        console.log(res);
        this.genre = res;
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }
}
