import { Component, OnInit } from '@angular/core';

import { LoadingController } from '@ionic/angular';
import { RestApiService } from '../rest-api.service';
import { ActivatedRoute, Router  } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators, FormArray, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {

  movieForm: FormGroup;
  genre: FormArray;

  constructor(controls: AbstractControl[],
    public api: RestApiService,
    public loadingController: LoadingController,
    private route: ActivatedRoute,
    public router: Router,
    private formBuilder: FormBuilder) {
      this.getMovie(this.route.snapshot.paramMap.get('id'));
      this.movieForm = this.formBuilder.group({
      'movie_name' : [null, Validators.required],
      'genre' : this.formBuilder.array([])
    });
  }

  ngOnInit() {
  }

  createGenre(): FormGroup {
    return this.formBuilder.group({
      movie_name: ''
    });
  }

  addBlankGenre(): void {
    this.genre = this.movieForm.get('genre') as FormArray;
    this.genre.push(this.createGenre());
  }

  deleteGenre(control, index) {
    control.removeAt(index);
  }

  async getMovie(id) {
    const loading = await this.loadingController.create({
      message: 'Loading'
    });
    await loading.present();
    await this.api.getMovieById(id).subscribe(res => {
      this.movieForm.controls['class_name'].setValue(res.class_name);
      const controlArray = <FormArray>this.movieForm.controls['genre'];
      res.genre.forEach(std => {
        controlArray.push(this.formBuilder.group({
           movie_name: ''
        }));
      });
      for (let i = 0; i < res.genre.length; i++) {
        controlArray.controls[i].get('movie_name').setValue(res.genre[i].movie_name);
      }
      console.log(this.movieForm);
      loading.dismiss();
    }, err => {
      console.log(err);
      loading.dismiss();
    });
  }

  async updateMovie() {
    await this.api.updateMovie(this.route.snapshot.paramMap.get('id'), this.movieForm.value)
    .subscribe(res => {
        const id = res['id'];
        this.router.navigate(['/detail', JSON.stringify(id)]);
      }, (err) => {
        console.log(err);
      });
  }
}
