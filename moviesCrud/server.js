// server.js
import express from 'express';
import dotenv from 'dotenv';
import 'babel-polyfill';
import MovieWithJsObject from './src/usingJSObject/controllers/Movies';
import MovieWithDB from './src/usingDB/controller/Movies';

dotenv.config();
const Movie = process.env.TYPE === 'db' ? MovieWithDB : MovieWithJsObject;
const app = express()

app.use(express.json())

app.get('/', (req, res) => {
  return res.status(200).send({'message': 'RIGHT ON!! Your endpoint is working'});
});

app.listen(3000)
console.log('app running on port ', 3000);