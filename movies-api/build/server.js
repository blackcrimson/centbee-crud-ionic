'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _dotenv = require('dotenv');

var _dotenv2 = _interopRequireDefault(_dotenv);

require('babel-polyfill');

var _Movies = require('./src/usingJSObject/controllers/Movies');

var _Movies2 = _interopRequireDefault(_Movies);

var _Movies3 = require('./src/usingDB/controller/Movies');

var _Movies4 = _interopRequireDefault(_Movies3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_dotenv2.default.config(); // server.js

var Movie = process.env.TYPE === 'db' ? _Movies4.default : _Movies2.default;
var app = (0, _express2.default)();

app.use(_express2.default.json());

app.get('/', function (req, res) {
  return res.status(200).send({ 'message': 'RIGHT ON!! Your endpoint is working' });
});

app.listen(3000);
console.log('app running on port ', 3000);